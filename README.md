# Advanced Web Publishing Apps

## Project

## Cork Institute of Technology

## November 2019

#### 1

## 1 Advanced Web Publishing Apps

## Project Assessment

This project contributes 40% in your final mark. This is an individual project and has to be all
done by yourself. You may be called to explain different parts of your submission, if needed.
The project has to be submitted via Canvas and it has to be a zip file named as follows:
IS1234567.zip where 1234567 is your student ID. So if your student ID is 435623 then the name
of the zip file would be IS435623. The zip file contain two files, one of them is the zip file of your
application folder (you should again use the same naming convention IS435623) and the other file
is the exported database file from Drupal. Your exported database file has the following naming
convention: DB435623 where 435623 is your student ID. Please do not use any other naming
convention. You should use theBackup and Migratemodule that we have discussed it in the
lecture in order to export your database.
You submission should also include a short screen recorded video with narration that can display
all the pages/block and etc you have created. You can use any screen recorder software you
want. The following link is a suggestion for a screen recorder. https://screencast-o-matic.
com/screen-recorder

### 1.1 Project Description

An example assessment would be to design and develop a web application that has a number of
different modular functionalities for a CMS.
The following shows two lists of modular functionalities:
The first list:

1. A simple module that creates a table in the Drupal database with 5 columns: ID, name, age,
   DOB (Data of Birth) and email address. The primary key is ID and the email address is
   unique. Populate the table with only one entry. The value of ID is generated automatically.
2. A simple calculator with 2 math operations: Addition and Multiplication.
3. A simple module to display the current time and date: Year, Month, Day, Hour, Minute,
   Second (PM/AM).
4. A multimedia module that has two radio buttons (labels: video 1, video 2) and two ordinary
   buttons (labels: Play, Close). The user may select one of the videos and then press the Play
   button to play the video. The Close button would close the video from the screen. Note:
   Videos are youtube videos.
5. A multimedia module that has two buttons (labels: Play, Close) and one text field. The end
   user may copy a youtube video link and paste it directly in the text field and press the Play
   button to play the video. The end user can close the video by pressing the Close button.

Note:The modules listed above may be developed as a block plugin or simple module (stand alone
page); however, there should be at least one module developed as a block plugin and at least one
module as a simple module (stand alone page).

Dr Farshad Ghassemi Toosi

```
Figure 1: An example of a form’s question
```

```
The second list:
```

1. Find an appropriate scenario forCAPTCHAmodule and show its functionality.
2. Create a web form using webform module. The web form will have the following features:
   - 4 pages. Each page should have buttons to navigate to the next and previous page (if
     possible).
   - The first page collects individual information: Age, gender (Male, Female, not-specified),
     city of born (Cork, Limerick, Dublin, Others: A text box to let the end user to type
     his/her city of born).
   - The second page has the following list of musics’ styles and lets the end user to specify
     how likely they would listen to a type of music: pop, classic, folk, rock, country. See
     Figure 1 as an example.
   - The third page has the following questions:
     (a) A list of images (4 images) of 4 different musical instruments. The end user can
     select their favorite musical instrument.
     (b) A table that contains a list of musical bands so that end user can select their favourite
     band(s).
   - The last page should let the end-user to sign the form (See Figure 2) and provide an
     email address.

```
No question is allowed to be unanswered.
```

3. Install and use google analytic module and link it to your google analytic account. Navigate
   to some pages in your web application and observe the list of visited pages in your google
   account and take a screenshot. The screenshot image needs to be submitted with the project.
4. Search and find one module of your choice that has not been discussed during the lecture.
   (Optional, extra mark for this task)

Create a content type and name it mymodules. For each module that you have developed, create
a content item and use the module as the second side-bar (if it is a block). Create a separate menu
item in the home page for each content item with an appropriate name. The paths of each content
item should be changed to an appropriate path.
Create a different role call it data analyst. The users from this role should have a permission to
work with webforms e.g., analysis the submitted data.

Dr Farshad Ghassemi Toosi

```
Figure 2: An example of a form’s question
```

### 1.2 Deadline and Submission

The deadline is on Monday 13th of December at 23:59 pm. Late submission would be accepted
with 10% penalty and the deadline for the late submission is 19th of December. Note: This is your
responsibility to make sure your submission works properly. I would recommend you to test your
submission (the exported database and folder) with another system before you submit. Please make
sure that all the pages and modules you have created are displayed in the video with a narration.

### 1.3 Rubric

This rubric is subject to change.

- Module development 60%:

```
1.Excellent100%: The student has successfully represented the scenario with the module
requirement.
2.
3.Good50%: The student has partially represented the scenario with the module require-
ment.
4.Poor0%: The student has not successfully represented the problem statement with the
module requirement.
```

- Installing and working with existing modules 40%:

```
1.Excellent100%: The student has successfully installed and used the module.
2.Good50%: The student has partially installed and used the module.
3.Poor0%: The student has not successfully installed and used the module.
```

Dr Farshad Ghassemi Toosi
