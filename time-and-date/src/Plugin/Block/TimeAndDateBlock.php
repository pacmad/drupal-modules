<?php
namespace Drupal\TimeAndDate\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Render\Markup;

$date = date("r");
$markup = "The date and time are: ".$date;

/**
 * Creates a 'Time And Date' Block
 * @Block(
 * id = "time_and_date_block",
 * admin_label = @Translation("Time And Date")
 */

 class TimeAndDate extends BlockBase{
    /**
     * {@inheritdoc}
     */
     public function getTimeAndDate(){
        return array(
                '#title' => 'Time And Date',
                '#markup' => $markup,
            );
     }
 }