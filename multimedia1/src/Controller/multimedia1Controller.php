<?php

namespace Drupal\multimedia1\src\Controller;

use Drupal\Core\Render\Markup;

class multimedia1Controller
{


   public function multimedia1()
   {
      $markup = <<<MARK
         <form >
            <p>Please select your gender:</p>
            <input type="radio" name="choice" value="video1"> video 1<br>
            <input type="radio" name="choice" value="video2"> video 2<br>
            <input type="submit" value="Choose" onclick="Choose()">
         </form>
         <input type="button" value="Play" onclick="Play()">
         <input type="button" value="Close" onclick="Close()">

            <div id="iframeContainer"/>
        


                     <script type="text/javascript">
                        function Choose(x = document.getElementsByName("choice").value)
                           if(x=='video1'){
                              document.getElementById("iframeContainer").innerHTML=`<iframe width="560" height="315" src="https://www.youtube.com/embed/PGhriTj1tOk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
                           }else(){
                              document.getElementById("iframeContainer").innerHTML=`<iframe width="560" height="315" src="https://www.youtube.com/embed/2atQnvunGCo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
                           }

                           function Close(){
                              document.getElementById("iframeContainer").innerHTML="";
                           }

                           Play(){
                              // it's already playing?
                           }
                     </script>

            MARK;


      return array(
         '#title' => 'multimedia 1',
         '#markup' => $markup
      );
   }
}
