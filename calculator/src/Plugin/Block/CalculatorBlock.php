<?php 
namespace Drupal\Calculator\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Render\Markup;

/**
 * Creates a 'calculator' Block
 * @Block(
 * id = "calculator_block",
 * admin_label = @Translation("Simple Calculator"),
 * )
 */

class Calculator extends BlockBase{
    /**
     * {@inheritdoc}
     */
    public function drawCalculator(){
        $markup = <<<MARK
            Calculator here ;)
            <input type='number' id='number1' />
            <input type='number' id='number2' />
            <input type='button' value = '+' id='add' onClick='add()'/>
            <input type='button'value='=' id='subtract' onClick='subtract()'/>
            <div id='answer' />
            <script type="text/javascript">
                func add(){
                    document.getElementById('answer').innerHtml = parseInt(document.getElementById('number1').value) + parseInt(document.getElementById('number2').value);
                }
                func subtract(){
                    document.getElementById('answer').innerHtml = parseInt(document.getElementById('number1').value) - parseInt(document.getElementById('number2').value);
                }
            </script>
        MARK;

          return array(
                '#title' => 'Simple Calculator',
                '#markup' => $markup,
            );
    }
}