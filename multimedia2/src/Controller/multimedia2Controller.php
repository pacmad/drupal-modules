<?php

namespace Drupal\multimedia2\src\Controller;

use Drupal\Core\Render\Markup;

class multimedia2Controller
{
   public function multimedia2()
   {
      $markup = <<<MARK
         <form>
            <p>Please enter the link to your YouTube Video:</p>
            <input type="text" id="pasteBox" />
            <input type="submit" value="Choose" onclick="Choose()">
            </form>
            <input type="button" value="Play" onclick="Play()">
            <input type="button" value="Close" onclick="Close()">
            <div id="iframeContainer"/>
         </form>


            <script type="text/javascript">
                  function Choose(x = document.getElementsByName("pasteBox").value){
                           document.getElementById("iframeContainer").innerHTML='<iframe width="560" height="315" src='"+x"+' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
                  }

                  function Close(){
                        document.getElementById("iframeContainer").innerHTML="";
                  }

                  Play(){
                     // it's already playing?
                  }
            </script>`

         MARK;


      return array(
         '#title' => 'multimedia 1',
         '#markup' => $markup
      );
   }
}
